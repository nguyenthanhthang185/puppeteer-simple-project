const puppeteer = require('puppeteer');

(async () => {
    //Mở 1 trình duyệt mới, headless (true, false): có/không mở trình duyệt
  const browser = await puppeteer.launch({headless: false});
  //Mở 1 thẻ mới
  const page = await browser.newPage();
  //Đi tới 1 trang web bằng url
  await page.goto('http://kenh14.vn');
  //Lưu vào đường dẫn
  await page.screenshot({path: './image/kenh14.png'});
  await browser.close();
})();